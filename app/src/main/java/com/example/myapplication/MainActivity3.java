package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity3 extends AppCompatActivity {

    Button bt1;
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        Log.d("PAPB1", "act 3 onCreate");
        bt1 = findViewById(R.id.bt1);
        tv1.setText("Hello world!");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("PAPB1", "act 3 onPause");
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("PAPB1", "act 3 onSaveInstanceState");
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.d("PAPB1", "act 3 onRestoreInstanceState");
    }

    // setelah stop, dia menjalankan onsaveInstanceState
}